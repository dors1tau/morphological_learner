from grammar import Grammar
from simulated_annealer import SimulatedAnnealing


def main():
    g = Grammar(variable_VI_map={'V': ['run', 'walk', 'runs', 'walks'],  # 'sprint', 'sprints', 'skip', 'skips'],
                                 'N': ['dog', 'dogs', 'cat', 'cats'],  # 'giraffe', 'giraffes', 'hippo', 'hippos'],  # 'lion', 'lions', 'shark', 'sharks'],
                                 'Det': ['these', 'that'],  # 'those', 'this'],
                                 # 'VP': [],
                                 'NP': [],
                                 'S': []},
                start_variable='S',
                rules=[('S', ['NP', 'V']),  # Note: rules must be binary
                       ('NP', ['Det', 'N']),
                       # ('VP', ['V'])
                       ])
    # g = Grammar(variable_VI_map={'V' : [],
    #                              'V-root': ['run', 'walk'],  #, 'sprint', 'skip'],
    #                              'V-agr': ['s', ''],
    #                              'N': [],
    #                              'N-root': ['dog', 'cat'],  # 'giraffe', 'hippo'],
    #                              'N-agr': ['s', ''],
    #                              'Det': ['these', 'that'],  # 'those', 'this'],
    #                              # 'VP': [],
    #                              'NP': [],
    #                              'S': []},
    #             start_variable='S',
    #             rules=[('S', ['NP', 'V']),  # Note: rules must be binary
    #                    ('NP', ['Det', 'N']),
    #                    ('N', ['N-root', 'N-agr']),
    #                    ('V', ['V-root', 'V-agr'])
    #                    # ('VP', ['V'])
    #                    ])

    sentences = ['that cat runs',
                 'that cat walks',
                 'these cats run',
                 'these cats walk',
                 'that dog walks',
                 'that dog runs',
                 'these dogs walk',
                 'these dogs run']
                 # 'a giraffe runs',
                 # 'a giraffe walks',
                 # 'the giraffes run',
                 # 'the giraffes walk',
                 # 'a hippo runs',
                 # 'a hippo walks',
                 # 'the hippos run',
                 # 'the hippos walk',
                 # 'a lion runs',
                 # 'a lion walks',
                 # 'the lions run',
                 # 'the lions walk',
                 # 'a shark runs',
                 # 'a shark walks',
                 # 'the sharks run',
                 # 'the sharks walk'
                 # ]

    # sentences = [['that', 'cat', '', 'run', 's'],
    #              ['that', 'cat', '', 'walk', 's'],
    #              ['these', 'cat', 's', 'run', ''],
    #              ['these', 'cat', 's', 'walk', ''],
    #              ['that', 'dog', '', 'walk', 's'],
    #              ['that', 'dog', '', 'run', 's'],
    #              ['these', 'dog', 's', 'walk', ''],
    #              ['these', 'dog', 's', 'run', '']]

    sentences_literals = [(sentence, [(word, var.name) for word, var in g.cky_parser(sentence).get_literals()]) for
                          sentence in sentences]

    initial_temperature = 100
    temperature_threshold = 10**-3
    cooling_rate = 0.95
    annealer = SimulatedAnnealing(initial_temperature, temperature_threshold, cooling_rate)
    final_hypothesis = annealer.run(g, sentences_literals)

    print('Final hypothesis:\n{}'.format(final_hypothesis))
    print('Final hypothesis\'s cost: {}'.format(final_hypothesis.cost))


if __name__ == '__main__':
    main()
