from errors import SetFeatureValueStatementsValuesClashError
from feature_statements import VariableFeatureDuo
from lexical_item import LexicalItem


class Node:
    def __init__(self, key, children=None, rule=None):
        self.key = key
        self.children = []
        if children:
            self.children = list(children)
        self.rule = rule

        self.features_to_code = {}

    def __repr__(self):
        if self.children:
            return '[{} {}]'.format(self.key, ' '.join(map(repr, self.children)))
        else:
            return self.key

    @property
    def variables(self):
        return [self.key] + [child.key for child in self.children]

    def get_literals(self):
        def get_literals_rec(node, literals_lst):
            if type(node.children[0]) == LexicalItem:  # i.e., not Node
                literals_lst.append((node.children[0], node.key))
                return
            assert all(type(child) == Node for child in node.children)
            for child in node.children:
                get_literals_rec(child, literals_lst)

        literals = []
        get_literals_rec(self, literals)
        return literals

    def get_leaves(self):
        def get_leaves_rec(node, literals_lst):
            if type(node.children[0]) == LexicalItem:  # i.e., not Node
                literals_lst.append(node)
                return
            assert all(type(child) == Node for child in node.children)
            for child in node.children:
                get_leaves_rec(child, literals_lst)

        leaves = []
        get_leaves_rec(self, leaves)
        return leaves

    def apply_rules(self):
        values_duos = {}
        id_values_counter = 0
        cur_nodes = [self]
        while cur_nodes:
            next_nodes = []
            for node in cur_nodes:
                if node.rule is not None:
                    assert len(node.children) > 0 and all(type(child) == Node for child in node.children)
                    assert len(node.variables) == len(node.rule.variables)

                    next_nodes.extend(node.children)

                    for identity_statement in node.rule.identity_feature_statements:
                        old_values = {node.variables[node.rule.variables.index(duo.variable)].features[duo.feature] for duo in identity_statement.variable_feature_duos if node.variables[node.rule.variables.index(duo.variable)].features[duo.feature] != ''}
                        old_id_values = {value for value in old_values if type(value) == str and value.startswith('Z')}
                        old_actual_values = {value for value in old_values if type(value) != str or not value.startswith('Z')}
                        assert len(old_values) <= 1 and len(old_actual_values) <= 1
                        if len(old_actual_values) == 1:
                            value = old_actual_values.pop()
                        elif len(old_id_values) == 1:
                            value = old_id_values.pop()
                        else:
                            id_values_counter += 1
                            value = 'Z{}'.format(id_values_counter)
                        if value not in values_duos:
                            values_duos[value] = set()
                        for duo in identity_statement.variable_feature_duos:
                            node.variables[node.rule.variables.index(duo.variable)].set_feature_value(duo.feature, value)
                            values_duos[value].add(VariableFeatureDuo(node.variables[node.rule.variables.index(duo.variable)], duo.feature))

                    for set_statement in node.rule.set_feature_statements:
                        old_value = node.variables[node.rule.variables.index(set_statement.variable_feature_duo.variable)].features[set_statement.variable_feature_duo.feature]
                        if set_statement.value not in values_duos:
                            values_duos[set_statement.value] = set()
                        if old_value != '':
                            if not ((type(old_value) == str and old_value.startswith('Z')) or (old_value == set_statement.value)):
                                raise SetFeatureValueStatementsValuesClashError()
                            for duo in values_duos[old_value]:
                                duo.variable.set_feature_value(duo.feature, set_statement.value)
                            values_duos[set_statement.value] = values_duos[set_statement.value].union(values_duos[old_value])
                            values_duos[old_value] = set()
                        else:
                            node.variables[node.rule.variables.index(set_statement.variable_feature_duo.variable)].set_feature_value(set_statement.variable_feature_duo.feature,
                                                                                                      set_statement.value)
                            values_duos[set_statement.value].add(VariableFeatureDuo(node.variables[node.rule.variables.index(set_statement.variable_feature_duo.variable)],
                                                                                 set_statement.variable_feature_duo.feature))
                else:
                    assert len(node.children) == 1 and type(node.children[0]) == LexicalItem

            cur_nodes = next_nodes


class Tree:
    def __init__(self, root=None):
        self.root = root

    def __repr__(self):
        return '{}'.format(repr(self.root))

    def get_literals(self):
        return self.root.get_literals()

    def get_leaves(self):
        return self.root.get_leaves()

    def apply_rules(self):
        self.root.apply_rules()
