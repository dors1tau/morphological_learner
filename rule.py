import copy
import random

from errors import MorphologicalLearnerError, ChangingRuleDidNotGoThroughError, NotEnoughStatementsError, \
    NotEnoughVariableFeatureDuosError, NoSuitableIdentityStatementToSplitError, CannotMergeAnyIdentityStatements, \
    CannotAddNewSetStatementError, CannotAddNewIdentityStatementError, StatementDoesNotExistError, \
    StatementAlreadyExistsError, IndexOfVariableOutOfRangeError, FeatureDoesNotExistForVariableError, \
    EquivalentStatementAlreadyExistsError, NoChangeOfStatementSoughtError, ClashingStatementsError
from feature_statements import VariableFeatureDuo, FeatureValuesIdentityStatement, FeatureValueSetStatement

NUM_TRIES = 100


class Rule(object):
    def __init__(self, variable, derivation):
        self.variable = variable
        self.derivation = derivation
        self.set_feature_statements = []
        self.identity_feature_statements = []

    @property
    def feature_statements(self):
        return self.set_feature_statements + self.identity_feature_statements

    @property
    def variables(self):
        return [self.variable] + self.derivation

    @property
    def available_variable_feature_duos_by_variable(self):
        free_duos_by_variable = {}

        duos_in_use = {statement.variable_feature_duo for statement in self.set_feature_statements}.union(
            set().union(*[statement.variable_feature_duos for statement in self.identity_feature_statements]))
        for variable in self.variables:
            cur_all_duos = {VariableFeatureDuo(variable, feature) for feature in variable.features}
            cur_available_duos = cur_all_duos.difference(duos_in_use)
            if len(cur_available_duos) > 0:
                free_duos_by_variable[variable] = cur_available_duos

        return free_duos_by_variable

    def has_set_statement_with_duo(self, variable_feature_duo):
        return any(
            statement.get_variable_feature_duo() == variable_feature_duo for statement in self.set_feature_statements)

    def change(self):
        change_methods = [self.remove_some_statement,
                          self.change_some_set_statement,
                          self.change_some_identity_statement,
                          self.split_some_identity_statement,
                          self.merge_some_identity_statements,
                          self.add_some_set_statement,
                          self.add_some_identity_statement]

        for i in range(NUM_TRIES):
            try:
                chosen_change_method = random.choice(change_methods)
                print('\t\tchose following change method: {}'.format(chosen_change_method.__name__))
                chosen_change_method()
            except MorphologicalLearnerError:
                print('\t\t\twas not successful')
                continue
            print('\t\t\twas successful!')
            return
        raise ChangingRuleDidNotGoThroughError()

    def remove_some_statement(self):
        if len(self.feature_statements) == 0:
            raise NotEnoughStatementsError()

        statement = random.choice(self.feature_statements)
        self.remove_statement(statement)

    def change_some_set_statement(self):
        if len(self.set_feature_statements) == 0:
            raise NotEnoughStatementsError()

        statement = random.choice(self.set_feature_statements)
        statement.change()

    def change_some_identity_statement(self):
        if len(self.identity_feature_statements) == 0:
            raise NotEnoughStatementsError()

        statement = random.choice(self.identity_feature_statements)
        statement.change(available_variable_feature_duos_by_variable=self.available_variable_feature_duos_by_variable)

    def split_some_identity_statement(self):
        if len(self.identity_feature_statements) == 0:
            raise NotEnoughStatementsError()

        shuffled_statements = copy.copy(self.identity_feature_statements)
        random.shuffle(shuffled_statements)
        for statement in shuffled_statements:
            try:
                new_statement1, new_statement2 = statement.split()
                self.remove_statement(statement)
                self.add_statement(new_statement1)
                self.add_statement(new_statement2)
                return
            except NotEnoughVariableFeatureDuosError:
                continue

        raise NoSuitableIdentityStatementToSplitError()

    def merge_some_identity_statements(self):
        duos_in_identity_statements = set().union(
            *[statement.variable_feature_duos for statement in self.identity_feature_statements])

        duos_statements_dict = {
            duo: [statement for statement in self.identity_feature_statements if duo in statement.variable_feature_duos]
            for duo in duos_in_identity_statements}

        duos_that_appear_more_than_once = {duo for duo in duos_statements_dict if len(duos_statements_dict[duo]) > 1}

        if len(duos_that_appear_more_than_once) == 0:  # TODO: fix bug- this is always zero (because when adding a new
                                                       #       identity statement, it is made sure that the relevant
                                                       #       VariableFeatureDuos are not in previous use (this needs
                                                       #       to be thought of for the case of identity statements)
            raise CannotMergeAnyIdentityStatements()

        random_order_duos_list = list(duos_that_appear_more_than_once)
        random.shuffle(random_order_duos_list)
        chosen_duo = random.choice(random_order_duos_list)

        num_statements_to_merge = random.randint(2, len(duos_statements_dict[chosen_duo]))

        shuffled_statements = list(duos_statements_dict[chosen_duo])
        random.shuffle(shuffled_statements)
        statements_to_merge = shuffled_statements[:num_statements_to_merge]

        merged_statement = FeatureValuesIdentityStatement.merge_identity_statements(statements_to_merge)

        for statement in statements_to_merge:
            self.remove_statement(statement)
        self.add_statement(merged_statement)

    def add_some_set_statement(self):
        if len(self.available_variable_feature_duos_by_variable) == 0:
            raise CannotAddNewSetStatementError()

        var = random.choice(list(self.available_variable_feature_duos_by_variable))
        duo = random.choice(list(self.available_variable_feature_duos_by_variable[var]))

        self.add_statement(FeatureValueSetStatement(duo))

    def add_some_identity_statement(self):
        lengths = {duo.feature.num_values for duo in
                   set().union(*self.available_variable_feature_duos_by_variable.values())}

        randomised_lengths_list = list(lengths)
        random.shuffle(randomised_lengths_list)

        for length in randomised_lengths_list:
            cur_duos_by_variable = {var: {duo for duo in self.available_variable_feature_duos_by_variable[var] if
                                          duo.feature.num_values == length} for var in
                                    self.available_variable_feature_duos_by_variable}

            vars_to_pop = [var for var in cur_duos_by_variable if len(cur_duos_by_variable[var]) == 0]
            for var in vars_to_pop:
                cur_duos_by_variable.pop(var)

            if len(cur_duos_by_variable) < 2:
                continue

            random_order_list = list(cur_duos_by_variable)
            random.shuffle(random_order_list)

            num_duos = random.randint(2, len(cur_duos_by_variable))
            self.add_statement(FeatureValuesIdentityStatement(
                set(random.choice(list(cur_duos_by_variable[var])) for var in random_order_list[:num_duos])))

            return

        raise CannotAddNewIdentityStatementError()

    def remove_statement(self, statement):
        if statement not in self.feature_statements:
            raise StatementDoesNotExistError()

        if type(statement) == FeatureValueSetStatement:
            self.set_feature_statements.remove(statement)
        elif type(statement) == FeatureValuesIdentityStatement:
            self.identity_feature_statements.remove(statement)

    def add_statement(self, statement):
        if statement in self.feature_statements:
            raise StatementAlreadyExistsError()

        if type(statement) == FeatureValueSetStatement:
            self.set_feature_statements.append(statement)
        elif type(statement) == FeatureValuesIdentityStatement:
            self.identity_feature_statements.append(statement)

    def add_new_set_value_statement(self, variable_index, feature, value, override=False):
        if not variable_index < len(self.variables):
            raise IndexOfVariableOutOfRangeError()
        variable = self.variables[variable_index]

        if not variable.has_feature(feature):
            raise FeatureDoesNotExistForVariableError()

        variable_feature_duo = VariableFeatureDuo(variable, feature)
        new_statement = FeatureValueSetStatement(variable_feature_duo, value)

        previous_statements = [statement for statement in self.set_feature_statements if
                               new_statement.equivalent_to(statement)]
        assert len(previous_statements) <= 1, 'Expected not to find more than one relevant previous statements'
        if len(previous_statements) == 1:
            if not override:
                raise EquivalentStatementAlreadyExistsError()
            else:
                previous_statement = previous_statements[0]
                if new_statement == previous_statement:
                    raise NoChangeOfStatementSoughtError()
                else:
                    previous_statement.value = value
        else:  # len == 0

            if any([identity_statement.contains(variable_feature_duo) for identity_statement in
                    self.identity_feature_statements]):
                raise ClashingStatementsError()

            self.add_statement(new_statement)

    def add_new_identity_statement(self, variable_index_feature_tuples):
        # assert len(variable_index_feature_tuples) > 1

        for variable_index, feature in variable_index_feature_tuples:
            if not variable_index < len(self.variables):
                raise IndexOfVariableOutOfRangeError()

        variable_feature_duos = {VariableFeatureDuo(self.variables[variable_index], feature) for
                                 (variable_index, feature) in variable_index_feature_tuples}

        for variable_feature_duo in variable_feature_duos:
            if any([statement.variable_feature_duo == variable_feature_duo for statement in
                    self.set_feature_statements]):
                raise ClashingStatementsError()

        if any([variable_feature_duos.issubset(statement.variable_feature_duos) for statement in
                self.identity_feature_statements]):
            raise StatementAlreadyExistsError()

        new_duos = set()
        previous_statements = []
        for variable_feature_duo in variable_feature_duos:
            cur_previous_statements = [statement for statement in self.identity_feature_statements if
                                       statement.contains(variable_feature_duo)]
            assert len(cur_previous_statements) <= 1, \
                'The tuple ({}) in more than one different Identity statements of rule {}'.format(
                    variable_feature_duo, self)
            if len(cur_previous_statements) == 1:
                previous_statements.extend(cur_previous_statements)
            else:  # len == 0
                new_duos.add(variable_feature_duo)

        for statement in previous_statements:
            self.remove_statement(statement)

        statements_to_merge = previous_statements

        if len(statements_to_merge) > 0:
            merged_statement = FeatureValuesIdentityStatement.merge_identity_statements(statements_to_merge)
            for duo in new_duos:
                merged_statement.add(duo)
            self.add_statement(merged_statement)
        else:
            self.add_statement(FeatureValuesIdentityStatement(new_duos))

    def remove_statements_with_variable_feature(self, variable_proto, feature):
        for variable_instance in variable_proto.instances:
            cur_variable_feature_duo = VariableFeatureDuo(variable_instance, feature, force=True)
            for set_statement in [statement for statement in self.set_feature_statements if
                                  statement.variable_feature_duo == cur_variable_feature_duo]:
                self.remove_statement(set_statement)

        for variable_instance in variable_proto.instances:
            cur_variable_feature_duo = VariableFeatureDuo(variable_instance, feature, force=True)
            for identity_statement in [statement for statement in self.identity_feature_statements if
                                       statement.contains(cur_variable_feature_duo)]:
                try:
                    identity_statement.remove(cur_variable_feature_duo)
                except NotEnoughVariableFeatureDuosError:
                    self.remove_statement(identity_statement)

    def __repr__(self):
        copied_self = copy.deepcopy(self)
        for variable in copied_self.variables:
            for feature in variable.features:
                variable.set_feature_value(feature, '')

        try:
            for set_statement in copied_self.set_feature_statements:
                set_statement.variable_feature_duo.variable.set_feature_value(
                    set_statement.variable_feature_duo.feature, set_statement.value)
        except MorphologicalLearnerError as ee:
            raise ee

        counter = 0
        for identity_statement in copied_self.identity_feature_statements:
            counter += 1
            for variable_feature_duo in identity_statement.variable_feature_duos:
                try:
                    variable_feature_duo.variable.set_feature_value(variable_feature_duo.feature, 'x{}'.format(counter))
                except MorphologicalLearnerError as eee:
                    raise eee

        return '{} ---> {}'.format(copied_self.variable, ', '.join([str(v) for v in copied_self.derivation]))

    @property
    def cost_str(self):
        copied_self = copy.deepcopy(self)
        for variable in copied_self.variables:
            for feature in variable.features:
                variable.set_feature_value(feature, '')

        variables_str = ''.join([var.prototype.id_str for var in copied_self.variables])
        variables_str += '1' * len(copied_self.variable.prototype.id_str)  # sep

        sets = []
        for set_statement in copied_self.set_feature_statements:
            sets.append('1' + bin(copied_self.variables.index(set_statement.variable_feature_duo.variable))[2:].zfill(
                len(bin(len(copied_self.variables) - 1)[2:])) +
                        bin(set_statement.variable_feature_duo.variable.prototype.get_feature_position(
                            set_statement.variable_feature_duo.feature))[2:].zfill(
                            len(bin(len(set_statement.variable_feature_duo.variable.prototype.features) - 1)[2:])) +
                        bin(set_statement.value)[2:].zfill(
                            len(bin(set_statement.variable_feature_duo.feature.num_values - 1)[2:])))

        sets_str = ''.join(sets) + '0'

        ids = []
        for identity_statement in copied_self.identity_feature_statements:
            id_str = ''.join(['10' + bin(copied_self.variables.index(variable_feature_duo.variable))[2:].zfill(
                len(bin(len(copied_self.variables) - 1)[2:])) +
                              bin(variable_feature_duo.variable.prototype.get_feature_position(
                                  variable_feature_duo.feature))[2:].zfill(
                                  len(bin(len(variable_feature_duo.variable.prototype.features) - 1)[2:]))
                              for variable_feature_duo in identity_statement.variable_feature_duos])
            ids.append('11' + id_str)

        ids_str = ''.join(ids) + '00'

        return variables_str + sets_str + ids_str

    @property
    def cost(self):
        return len(self.cost_str)


class LexicalRule(Rule):
    def __init__(self, variable, derivation):
        super().__init__(variable, derivation)
