import copy
import itertools
import random
import string

from errors import FeaturesMismatchError, FeatureValueOutOfRangeError, NoVariableFeaturesError, \
    MorphologicalLearnerError, TooLittleInstancesExistInLexiconItemError, CannotAddInstanceToLexiconItem, \
    CannotChangeInstanceInLexiconItem, LexiconItemIsSaturatedError, FeatureDoesNotExistForVariableError, \
    FeatureExistsForVariableError


class LexicalItem(object):
    def __init__(self, word, variable, instances=None):
        if not instances:
            instances = [{}]
        if any(instance.keys() != variable.features.keys() for instance in instances):
            raise FeaturesMismatchError()

        for instance in instances:
            for feature in instance:
                if instance[feature] not in feature.values:
                    raise FeatureValueOutOfRangeError()

        self.word = word
        self.variable = variable
        self.instances = instances

        variable.add_lexical_item(self)

    @property
    def cost_str(self):
        '''
        :return: cost_str:  <word_str_5_bits_per_letter>00000 --->
                            (1<instance_str>)*0

                            instance_str = <f1_value_in_appropriate_num_of_bits><f1_value_in_appropriate_num_of_bits>...<fn_value_in_appropriate_num_of_bits>
        '''

        def word_to_binary(word):
            letters_dict = {letter: index + 1 for index, letter in enumerate(string.ascii_lowercase)}
            return ''.join([bin(letters_dict[letter])[2:].zfill(5) for letter in word])

        word_str = word_to_binary(self.word) + '00000'

        instances_features_strs = []
        for instance in self.instances:
            instance_features_str = ''
            for feature in instance:
                instance_features_str += bin(instance[feature])[2:].zfill(len(bin(feature.num_values - 1)[2:]))
            instance_features_str = '1' + instance_features_str
            instances_features_strs.append(instance_features_str)

        instances_str = ''.join(instances_features_strs) + '0'
        return word_str + instances_str

    @property
    def cost(self):
        return len(self.cost_str)

    @property
    def possible_instances(self, random_order=False):
        if len(self.variable.features) > 0:
            options = itertools.product(
                *[itertools.product([feature], feature.values) for feature in self.variable.features])
            if random_order:
                random.shuffle(options)
            for option in options:
                features = copy.copy(self.variable.features)
                features.update({feature: value for feature, value in option})
                yield features
        else:
            yield {}

    def change(self):
        if len(self.variable.features) == 0:
            raise NoVariableFeaturesError()

        change_methods = [self.add_instance,
                          self.remove_instance,
                          self.change_instance]

        random.shuffle(change_methods)

        for func in change_methods:
            try:
                print('\t\tchose following change method: {}'.format(func.__name__))
                func()
            except MorphologicalLearnerError:
                print('\t\t\twas not successful')
                continue
            print('\t\t\twas successful!')
            return
        assert False, 'Expected to be able to change a Lexicon Item!'

    def remove_instance(self):
        if len(self.instances) == 1:
            raise TooLittleInstancesExistInLexiconItemError()

        random_order_instances = list(self.instances)
        random.shuffle(random_order_instances)

        self.instances.remove(random_order_instances[0])

    def add_instance(self):
        if len(self.instances) == len(list(self.possible_instances)):
            raise CannotAddInstanceToLexiconItem()

        old_instance, new_instance = self.find_new_neighbour_instance()
        self.instances.append(new_instance)

    def change_instance(self):
        if len(self.instances) == len(list(self.possible_instances)):
            raise CannotChangeInstanceInLexiconItem()

        old_instance, new_instance = self.find_new_neighbour_instance()
        self.instances.remove(old_instance)
        self.instances.append(new_instance)

    def find_new_neighbour_instance(self):
        if len(self.instances) == len(list(self.possible_instances)):
            raise LexiconItemIsSaturatedError()

        random_order_features = list(self.variable.features)
        random.shuffle(random_order_features)

        random_order_instances = list(self.instances)
        random.shuffle(random_order_instances)

        for feature in random_order_features:
            for instance in random_order_instances:
                values_to_choose_from = [value for value in feature.values if not value == instance[feature]]
                random.shuffle(values_to_choose_from)
                for value in values_to_choose_from:
                    new_instance = copy.copy(instance)
                    new_instance[feature] = value
                    if new_instance not in self.instances:
                        return instance, new_instance

        assert False, 'Expected to be able to change an instance if not all instances exist!'

    def add_new_feature(self, feature):
        if not self.variable.has_feature(feature):
            raise FeatureDoesNotExistForVariableError()
        for instance in self.instances:
            instance[feature] = random.choice(feature.values)

    def remove_feature(self, feature):
        if self.variable.has_feature(feature):
            raise FeatureExistsForVariableError()
        for instance in self.instances:
            if feature not in instance:
                raise FeatureDoesNotExistForVariableError()
            instance.pop(feature)
        new_instances = []
        for instance in self.instances:
            if instance not in new_instances:
                new_instances.append(instance)
        self.instances = new_instances

    def __repr__(self):
        return '\'{}\' {}'.format(self.word, self.instances)

    def get_instances_by_feature_criteria(self, feature, value):
        return [instance for instance in self.instances if instance[feature] == value]

    def get_num_instances_by_features(self, features):
        return len([instance for instance in self.instances if all(instance[feature] == features[feature] for feature in features)])

    def get_instances_by_features(self, features):
        return [instance for instance in self.instances if all(instance[feature] == features[feature] for feature in features)]
