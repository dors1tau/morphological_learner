import random
import math


class ImproperNeighbourError(Exception):
    pass


class SimulatedAnnealing:
    def __init__(self, initial_temperature, threshold, cooling_rate):
        self.initial_temperature = initial_temperature
        self.threshold = threshold
        self.cooling_rate = cooling_rate

    def run(self, initial_hypothesis, data):
        h = initial_hypothesis
        t = self.initial_temperature

        num_iterations = 0
        while t > self.threshold:
            num_iterations += 1
            print('ITERATION #{}'.format(num_iterations))
            print('TEMPERATURE: {}'.format(t))

            print('Current hypothesis:\n')
            print(h)

            try:
                h2 = h.get_neighbour()

                h_energy = h.get_energy(data)
                h2_energy = h2.get_energy(data)
                delta = h2_energy - h_energy
            except ImproperNeighbourError as e:
                print("BAD NEIGHBOUR FOUND")
                print('\n\n#####################################################\n\n')
                print(h2)
                print('\n\n#####################################################\n\n')
                print('\n\n*****************************************************\n\n')
                continue

            print('H\'s energy: {}, Neighbour\'s energy:{}'.format(h_energy, h2_energy))
            print('GOOD NEIGHBOUR FOUND')
            print(h2)
            if delta < 0:
                p = 1
            else:
                p = math.exp(-delta * 1.0 / t)
            if random.random() <= p:
                print('SWITCHING TO NEIGHBOUR')
                h = h2
            t = self.cooling_rate * t
            print('\n\n*****************************************************\n\n')
        return h
